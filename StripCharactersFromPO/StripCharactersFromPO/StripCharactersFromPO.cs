﻿using System;
using Hyland.Unity;
using System.Text.RegularExpressions;

namespace StripCharactersFromPO
{
    public class StripCharactersFromPo : Hyland.Unity.IWorkflowScript
    {

        private const Diagnostics.DiagnosticsLevel DiagLevel = Diagnostics.DiagnosticsLevel.Verbose;
        private const string ScriptName = "String Characters From PO";

        #region Private Variables

        private Document _currentDocument;
        private const string PoKeyword = "PO Number";
        private const string Pattern = @"-[a-zA-Z0-9]*";

        #endregion

        #region IWorkflowScript
        /// <summary>
        /// Implementation of <see cref="IWorkflowScript.OnWorkflowScriptExecute" />.
        /// <seealso cref="IWorkflowScript" />
        /// </summary>
        /// <param name="app"><see cref="Application"/></param>
        /// <param name="args"><see cref="WorkflowEventArgs"/></param>
        public void OnWorkflowScriptExecute(Application app, WorkflowEventArgs args)
        {
            app.Diagnostics.Level = DiagLevel;
            app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("Start Script: {0}", ScriptName));

            try
            {
                _currentDocument = args.Document;
                if (_currentDocument == null)
                    throw new ApplicationException("Current document not found in workflow.");

                foreach (var keyRec in _currentDocument.KeywordRecords)
                {
                    Keyword key = keyRec.Keywords.Find(PoKeyword);

                    Match m = Regex.Match(key.Value.ToString(), Pattern, RegexOptions.IgnoreCase);

                    if (m.Success)
                    {
                        app.Diagnostics.Write(string.Format("Keyword value [{0}] matched pattern.", key.Value.ToString()));
                        KeywordModifier keyMod = _currentDocument.CreateKeywordModifier();
                        Regex rgx = new Regex(Pattern);
                        string newValue = rgx.Replace(key.Value.ToString(), "");
                        Keyword newKeyword = CreateKeywordHelper(key.KeywordType, newValue);
                        app.Diagnostics.Write(string.Format("New Keyword value [{0}]", newValue));
                        keyMod.UpdateKeyword(key, newKeyword);
                        keyMod.ApplyChanges();
                    }
                    else
                    {
                        app.Diagnostics.Write(string.Format("Keyword value [{0}] did not match pattern.", key.Value.ToString()));
                    }
                }

                args.ScriptResult = true;
            }
            catch (Exception ex)
            {
                if ((app.Diagnostics != null))
                {
                    string error = String.Format("{0} (Script: {1}) (User: {2}) (Document: {3})",
                        ex.GetType().Name,
                        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,
                        app.CurrentUser.Name,
                        args.Document.ID);

                    app.Diagnostics.Write(error);

                    app.Diagnostics.Write(ex);
                    app.Diagnostics.Write(ex.StackTrace);
                    args.ScriptResult = false;
                }
            }
            finally
            {
                app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("End Script: {0}", ScriptName));
            }
        }
        #endregion

        #region Helper Methods

        private Keyword CreateKeywordHelper(KeywordType Keytype, string Value)
        {
            Keyword key = null;
            switch (Keytype.DataType)
            {
                case KeywordDataType.Currency:
                case KeywordDataType.Numeric20:
                    decimal decVal = decimal.Parse(Value);
                    key = Keytype.CreateKeyword(decVal);
                    break;
                case KeywordDataType.Date:
                case KeywordDataType.DateTime:
                    DateTime dateVal = DateTime.Parse(Value);
                    key = Keytype.CreateKeyword(dateVal);
                    break;
                case KeywordDataType.FloatingPoint:
                    double dblVal = double.Parse(Value);
                    key = Keytype.CreateKeyword(dblVal);
                    break;
                case KeywordDataType.Numeric9:
                    long lngVal = long.Parse(Value);
                    key = Keytype.CreateKeyword(lngVal);
                    break;
                default:
                    key = Keytype.CreateKeyword(Value);
                    break;
            }
            return key;
        }

        #endregion
    }
}
