' Used to force variable declaration before usage
Option Explicit

'Thick client entry point
Sub Main35()
On Error Resume Next ' Enable Error Handling
	Dim objApplication, objCurrentDocument, objKeywords, objKeyword, i
	' Root level OnBase Automation object
	Set objApplication = CreateObject("OnBase.Application")
	' Currently active document returned by the root automation object
	Set objCurrentDocument = objApplication.CurrentDocument
	' Keyword collection for the current document
	Set objKeywords = objCurrentDocument.Keywords
	
	For i = 0 to objKeywords.Count - 1
		Set objKeyword = objKeywords.Item(i)
		If objKeyword.Name = "PO Number" Then
			Dim regex
			Set regex = new regexp
			regex.Pattern = "-[a-zA-Z0-9]*"	'Match exactly 1 hyphen, followed by one or more word character [a-zA-Z_0-9]
			regex.IgnoreCase = True
			
			If regex.Test(objKeyword.Value) = True Then
				objKeyword.Value = regex.Replace(objKeyword.Value, "")
				Call objCurrentDocument.StoreKeywords()
			End If
			
			Exit For
		End If
	Next
End Sub